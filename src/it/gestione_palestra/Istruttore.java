package it.gestione_palestra;

public class Istruttore extends Membro{
    private String specializzazione;
    private int anniEsperienza;

    /**
     * Costruttore che definisce un oggetto della classe Istruttore che estende la classe Membro.
     * @param nome Stringa che rappresenta il nome dell'istruttore.
     * @param cognome Stringa che rappresenta il cognome dell'istruttore.
     * @param specializzazione Stringa che rappresenta la specializzazione dell'istruttore.
     * @param anniEsperienza Intero che rappresenta gli anni di esperienza dell'istruttore.
     */
    public Istruttore(String nome, String cognome, String specializzazione, int anniEsperienza) {
        super(nome, cognome);
        this.specializzazione = specializzazione;
        this.anniEsperienza = anniEsperienza;
    }
    /**
     * Metodo che ritorna la specializzazione dell'istruttore.
     * @return La stringa che rappresenta la specializzazione dell'istruttore.
     */
    public String getSpecializzazione() {
        return this.specializzazione;
    }
    /**
     * Metodo che modifica la specializzazione dell'istruttore.
     * @param specializzazione Stringa che rappresenta la nuova specializzazione dell'istruttore.
     */
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }
    /**
     * Metodo che ritorna gli anni di esperienza dell'istruttore.
     * @return La stringa che rappresenta gli anni di esperienza dell'istruttore.
     */
    public int getAnniEsperienza() {
        return this.anniEsperienza;
    }
    /**
     * Metodo che modifica gli anni di esperienza dell'istruttore.
     * @param anniEsperienza Intero che rappresenta i nuovi anni di esperienza.
     */
    public void setAnniEsperienza(int anniEsperienza) {
        this.anniEsperienza = anniEsperienza;
    }

    /**
     * Metodo che ritorna la stringa che descrive un istruttore.
     * @return La stringa che descrive l'istruttore.
     */
    @Override
    public String toString() {
        return "L'istruttore " + super.getNome() + " " + super.getCognome();
    }
}
