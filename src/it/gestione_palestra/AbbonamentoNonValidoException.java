package it.gestione_palestra;

/**
 * Eccezione che gestisce il caso in cui un membro con abbonamento scaduto tenta di iscriversi a una classe della palestra.
 */
public class AbbonamentoNonValidoException extends Exception{
    public AbbonamentoNonValidoException() {
        super();
    }
    public AbbonamentoNonValidoException(String message) {
        super(message);
    }
}
