package it.gestione_palestra;

/**
 * Abbonamento è un tipo di variabile enumerata che rappresenta i tipi di abbonamento disponibili in palestra.
 */
public enum Abbonamento {
    GIORNALIERO,
    MENSILE,
    ANNUALE,
    SCADUTO;
}
