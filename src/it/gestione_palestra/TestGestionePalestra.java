package it.gestione_palestra;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Map;

public class TestGestionePalestra {
    public static void main(String[] args) {
        // Creazione palestra
        GestionePalestra palestra = new GestionePalestra();
        // Creazione membri
        Membro membro1 = new Membro("Mattia", "Consonni");
        Membro membro2 = new Membro("Fabrizio", "Pesce");
        Membro membro3 = new Membro("Anna", "Crippa");
        Membro membro4 = new Membro("Lorenzo", "Abate");
        Membro membro5 = new Membro("Pio", "D'Anna");
        Membro membro6 = new Membro("Walter", "Garcia");
        Membro membro7 = new Membro("Alessandro", "Masetti");
        // Creazione istruttori
        Istruttore istruttore1 = new Istruttore("Paolo", "Primo", "Calistenichs", 3);
        Istruttore istruttore2 = new Istruttore("Paolo", "Secondo", "Power-lifting", 2);
        Istruttore istruttore3 = new Istruttore("Paolo", "Primo", "Zumba", 30);
        // Creazione date iscrizione
        LocalDate data1 = LocalDate.of(2024, 4, 8);
        LocalDate data2 = LocalDate.of(2023, 7, 8);
        LocalDate data3 = LocalDate.of(1983, 9, 4);
        // Aggiunta Membri
        palestra.aggiungiMembro(membro1, Abbonamento.MENSILE, data1);
        palestra.aggiungiMembro(membro2, Abbonamento.ANNUALE, data2);
        palestra.aggiungiMembro(membro5, Abbonamento.ANNUALE, data2);
        palestra.aggiungiMembro(membro3, Abbonamento.ANNUALE, data3);
        palestra.aggiungiMembro(membro4, Abbonamento.GIORNALIERO, data1);
        palestra.aggiungiMembro(membro6, Abbonamento.ANNUALE, data2);
        palestra.aggiungiMembro(membro7, Abbonamento.ANNUALE, data3);
        // Creazioni classi di allenamento
        ClasseDiAllenamento zumba = new ClasseDiAllenamento("Zumba", "18:00", 15, DayOfWeek.WEDNESDAY);
        ClasseDiAllenamento powerLifting = new ClasseDiAllenamento("Power Lifting", "15:00", 2, DayOfWeek.WEDNESDAY);
        ClasseDiAllenamento calistenichs = new ClasseDiAllenamento("Calistenichs", "18:00", 20, DayOfWeek.FRIDAY);
        ClasseDiAllenamento crossfit = new ClasseDiAllenamento("Crossfit", "19:00", 10, DayOfWeek.SATURDAY);
        // Aggiunta Classi in palestra
        palestra.aggiungiClasseconIstruttore(zumba, istruttore3);
        palestra.aggiungiClasseconIstruttore(calistenichs, istruttore1);
        palestra.aggiungiClasseconIstruttore(powerLifting, istruttore2);
        // Iscrizione membri con errori senza eccezioni
        try {
            palestra.iscrizioneMembriClassi(new Membro("Paolo", "Quarto"), zumba);
            palestra.iscrizioneMembriClassi(membro1, crossfit);
            palestra.iscrizioneMembriClassi(membro1, powerLifting);
            palestra.iscrizioneMembriClassi(membro2, powerLifting);
            palestra.iscrizioneMembriClassi(membro4, zumba);
            palestra.iscrizioneMembriClassi(membro1, calistenichs);
            palestra.iscrizioneMembriClassi(membro6, zumba);
        } catch (ClassePienaException e) {
            System.out.println("Spiacente, la classe è piena!");
        } catch (AbbonamentoNonValidoException e) {
            System.out.println("Abbonamento scaduto!");
        }
        // Caso con eccezione ClassePienaException
        try {
            palestra.iscrizioneMembriClassi(membro4, powerLifting);
        } catch (ClassePienaException e) {
            System.out.println("Spiacente, la classe è piena!");
        } catch (AbbonamentoNonValidoException e) {
            System.out.println("Abbonamento scaduto!");
        }
        // Caso con eccezione AbbonamentoScadutoException
        try {
            palestra.iscrizioneMembriClassi(membro7, zumba);
        } catch (ClassePienaException e) {
            System.out.println("Spiacente, la classe è piena!");
        } catch (AbbonamentoNonValidoException e) {
            System.out.println("Abbonamento scaduto!");
        }
        // Stampo i corsi erogati il mercoledì
        palestra.mostraClassi(DayOfWeek.WEDNESDAY);
        // Chiedo le recensione utente del corso
        palestra.recensioneCorso(zumba);
    }
}
