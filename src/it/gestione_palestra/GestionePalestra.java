package it.gestione_palestra;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

public class GestionePalestra {
    private Map<String, Abbonamento> abbonamenti;
    private List<ClasseDiAllenamento> classiDiAllenamenti;
    // Contatore per la gestione del id dei membri in seguito alla iscrizione in palestra
    private int idGenerator = 1;

    /**
     * Costruttore di un oggetto GestionePalestra che inizializza un hashmap di abbonamenti e una lista di classi disponibili.
     */
    public GestionePalestra() {
        this.abbonamenti = new HashMap<>();
        this.classiDiAllenamenti = new ArrayList<>();
    }

    /**
     * Metodo che si occupa di aggiungere un membro in palestra, specificando l'abbonamento e la data d'iscrizione.
     * @param membro Il membro che si vuole aggiungere.
     * @param abbonamento L'abbonamento che vuole scegliere il membro.
     * @param localDate La data d'iscrizione del membro in palestra.
     */
    public void aggiungiMembro(Membro membro, Abbonamento abbonamento, LocalDate localDate) {
        membro.setAbbonamento(abbonamento);
        membro.setGiornoIscrizione(localDate);
        membro.setId(Integer.toString(this.idGenerator++));
        this.abbonamenti.put(membro.getId(), membro.getAbbonamento());
    }

    /**
     * Metodo che aggiunge alla palestra una classe di allenamento con il seguente istruttore associato.
     * @param classeDiAllenamento Classe di allenamento che si vuole aggiungere alla palestra.
     * @param istruttore Istruttore che si occuperà di erogare l'insegnamento.
     */
    public void aggiungiClasseconIstruttore(ClasseDiAllenamento classeDiAllenamento, Istruttore istruttore) {
        classeDiAllenamento.setIstruttore(istruttore);
        this.classiDiAllenamenti.add(classeDiAllenamento);
    }

    /**
     * Metodo che gestisce l'iscrizione di un membro al corso, controllando se il membro è iscritto in palesstra e se il corso
     * è erogato da esso. Inoltre gestisce il caso in cui la classe dovesse essere piena e il caso in cui l'abbonamento è scaduto.
     * @param membro Membro che si vuole iscrivere al corso.
     * @param classeDiAllenamento Classe dove si vuole iscrivere al membro.
     * @throws ClassePienaException Viene lanciata questa eccezione quando la classe ha finito la capienza.
     * @throws AbbonamentoNonValidoException Viene lanciata quando l'abbonamento risulta essere scaduto alla data corrente.
     */
    public void iscrizioneMembriClassi(Membro membro, ClasseDiAllenamento classeDiAllenamento) throws ClassePienaException, AbbonamentoNonValidoException{
        // Controllo se la capacità degli studenti è stata esaurita o meno
        if (classeDiAllenamento.getCapacita() == 0)
            throw new ClassePienaException();
        // Controllo se il membro è registrato in palestra
        if (!this.abbonamenti.containsKey(membro.getId()))
            System.out.println("Non esiste alcun membro associato!");
        // Controllo se la classa è erogata in palestra
        else if (!this.classiDiAllenamenti.contains(classeDiAllenamento))
            System.out.println("La classe specificata non esiste!");
        // Controllo della validità dell'abbonamento
        else if (!controlloValiditaAbbonamento(membro)) {
            membro.setAbbonamento(Abbonamento.SCADUTO);
            throw new AbbonamentoNonValidoException();
        }
        else {
            classeDiAllenamento.getIscritti().add(membro);
            classeDiAllenamento.setCapacita(classeDiAllenamento.getCapacita() - 1);
        }
    }

    /**
     * Metodo che controlla se l'abbonamento del membro è valido o meno.
     * @param membro Membro di cui si vuole controllare l'abbonamento.
     * @return Vero se l'abbonamento è valido oppure falso se scaduto.
     */
    private boolean controlloValiditaAbbonamento(Membro membro) {
        switch (membro.getAbbonamento()) {
            case GIORNALIERO:
                return !((membro.getGiornoIscrizione().plusDays(1)).isBefore(LocalDate.now()));
            case MENSILE:
                return !((membro.getGiornoIscrizione().plusMonths(1)).isBefore(LocalDate.now()));
            case ANNUALE:
                return !((membro.getGiornoIscrizione().plusYears(1)).isBefore(LocalDate.now()));
            default:
                return false;
        }
    }

    /**
     * Metodo che ritorna gli abbonamenti della palestra.
     * @return L'HashMap che contiene il mapping id Membri e abbonamenti.
     */
    public Map<String, Abbonamento> getAbbonamento() {
        return this.abbonamenti;
    }
    /**
     * Metodo che modifica gli abbonamenti della palestra.
     * @param abbonamento Nuova mappa di abbinamenti id Membro e abbonamento.
     */
    public void setAbbonamento(Map<String, Abbonamento> abbonamento) {
        this.abbonamenti = abbonamento;
    }
    /**
     * Metodo che ritorna la lista delle classi erogate in palestra.
     * @return La lista dei corsi disponibili.
     */
    public List<ClasseDiAllenamento> getClassiDiAllenamenti() {
        return this.classiDiAllenamenti;
    }
    /**
     * Metodo che modifica la lista degli insegnamenti disponibili.
     * @param classiDiAllenamenti Nuova lista di insegnamenti.
     */
    public void setClassiDiAllenamenti(List<ClasseDiAllenamento> classiDiAllenamenti) {
        this.classiDiAllenamenti = classiDiAllenamenti;
    }

    /**
     * Metodo che stampa tutti i corsi erogati durante il giorno della settimana scelto.
     * @param dayOfWeek Il giorno della settimana dove vengono erogati i corsi.
     */
    public void mostraClassi(DayOfWeek dayOfWeek) {
        for (ClasseDiAllenamento classeDiAllenamento: this.classiDiAllenamenti) {
            // Controllo sul giorno in cui viene erogato il corso
            if (dayOfWeek == classeDiAllenamento.getDayOfWeek()) {
                System.out.println(classeDiAllenamento.getIstruttore().toString() + " insegna il corso " + classeDiAllenamento.getNomeClasse() + "\n" +
                                    "Numero di posti rimanenti: " + classeDiAllenamento.getCapacita());
            }
        }
    }

    /**
     * Metodo che permette a ogni singolo membro di un corso di poterlo valutare inserendo da tastiera il proprio voto
     * così da poter calcolare la media finale.
     * @param classeDiAllenamento Classe di allenamento di cui si vogliono inserire i voti e calcolare la media.
     */
    public void recensioneCorso(ClasseDiAllenamento classeDiAllenamento) {
        Scanner in = new Scanner(System.in);
        int sum = 0;
        for (int i = 0; i < classeDiAllenamento.getIscritti().size(); i++) {
            System.out.println("Voto del membro: " + classeDiAllenamento.getIscritti().get(i).toString());
            sum += Integer.parseInt(in.nextLine());
        }
        System.out.println("La media delle recensioni è: " + sum / (double) classeDiAllenamento.getIscritti().size());
    }
}
