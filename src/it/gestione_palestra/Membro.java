package it.gestione_palestra;

import java.time.LocalDate;

public class Membro {
    private String nome;
    private String cognome;
    private String id;
    private Abbonamento abbonamento;
    private LocalDate giornoIscrizione;

    /**
     * Costruttore che inizializza un membro con il proprio nome e cognome.
     * @param nome Stringa che rappresenta il nome.
     * @param cognome Stringa che rappresenta il cognome.
     */
    public Membro(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    /**
     * Metodo che ritorna il nome del membro.
     * @return La stringa che rappresenta il nome del membro.
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Metodo che modifica il nome del membro.
     * @param nome Stringa che rappresenta il nuovo nome.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Metodo che ritorna il cognome del membro.
     * @return La stringa che rappresenta il cognome del membro.
     */
    public String getCognome() {
        return this.cognome;
    }
    /**
     * Metodo che modifica il cognome del membro.
     * @param cognome Stringa che rappresenta il nuovo cognome.
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    /**
     * Metodo che ritorna l'id del membro.
     * @return La stringa che rappresenta l'id del membro.
     */
    public String getId() {
        return this.id;
    }
    /**
     * Metodo che modifica l'id del membro.
     * @param id Stringa che rappresenta il nuovo id.
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * Metodo che ritorna l'abbonamento del membro.
     * @return L'abbonamento corrente del membro.
     */
    public Abbonamento getAbbonamento() {
        return this.abbonamento;
    }
    /**
     * Metodo che modifica l'abbonamento del membro.
     * @param abbonamento Stringa che rappresenta il nuovo abbonamento.
     */
    public void setAbbonamento(Abbonamento abbonamento) {
        this.abbonamento = abbonamento;
    }
    /**
     * Metodo che ritorna il giorno d'iscrizione del membro.
     * @return Ritorna il giorno d'iscrizione alla palestra.
     */
    public LocalDate getGiornoIscrizione() {
        return giornoIscrizione;
    }
    /**
     * Metodo che modifica la data d'iscrizione del membro.
     * @param giornoIscrizione Stringa che rappresenta il nuovo nome.
     */
    public void setGiornoIscrizione(LocalDate giornoIscrizione) {
        this.giornoIscrizione = giornoIscrizione;
    }

    /**
     * Metodo che crea la stringa che descrive l'oggetto membro
     * @return La stringa contenente le informazioni principali dell'oggetto membro.
     */
    @Override
    public String toString() {
        return this.nome + " " + this.cognome;
    }
}
