package it.gestione_palestra;

/**
 * Eccezione che gestisce il caso in cui un membro cerca di iscriversi a un corso già pieno.
 */
public class ClassePienaException extends Exception{
    public ClassePienaException() {
        super();
    }

    public ClassePienaException(String message) {
        super(message);
    }
}
