package it.gestione_palestra;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public class ClasseDiAllenamento {
    private String nomeClasse;
    private DayOfWeek dayOfWeek;
    private String orario;
    private List<Membro> iscritti;
    private Istruttore istruttore;
    private int capacita;

    /**
     * @param nomeClasse Stringa che rappresenta il nome della classe.
     * @param orario Stringa che rappresenta 'orario di inizio del corso.
     * @param capacita Intero che rappresenta la quantità di membri che possono partecipare.
     * @param dayOfWeek Giorno della settimana di quando viene erogato il corso.
     */
    public ClasseDiAllenamento(String nomeClasse, String orario, int capacita, DayOfWeek dayOfWeek) {
        this.nomeClasse = nomeClasse;
        this.orario = orario;
        this.iscritti = new ArrayList<>();
        this.capacita = capacita;
        this.dayOfWeek = dayOfWeek;
    }
    /**
     * Metodo che ritorna il nome della classe.
     * @return La stringa che rappresenta il nome della classe.
     */
    public String getNomeClasse() {
        return this.nomeClasse;
    }
    /**
     * Metodo che modifica il nome della classe.
     * @param nomeClasse Stringa che rappresenta il nuovo nome della classe.
     */
    public void setNomeClasse(String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }
    /**
     * Metodo che ritorna l'orario della lezione.
     * @return La stringa che rappresenta l'orario della lezione.
     */
    public String getOrario() {
        return this.orario;
    }
    /**
     * Metodo che modifica l'orario della lezione.
     * @param orario Stringa che rappresenta il nuovo orario.
     */
    public void setOrario(String orario) {
        this.orario = orario;
    }
    /**
     * Metodo che ritorna la lista dei membri iscritti.
     * @return La lista dei membri iscritti alla classe
     */
    public List<Membro> getIscritti() {
        return this.iscritti;
    }
    /**
     * Metodo che modifica la lista degli iscritti.
     * @param iscritti Lista che rappresenta i nuovi iscritti.
     */
    public void setIscritti(List<Membro> iscritti) {
        this.iscritti = iscritti;
    }
    /**
     * Metodo che ritorna l'istruttore del corso.
     * @return L'istruttore del corso.
     */
    public Istruttore getIstruttore() {
        return this.istruttore;
    }
    /**
     * Metodo che modifica l'istruttore del corso.
     * @param istruttore Nuovo istruttore del corso.
     */
    public void setIstruttore(Istruttore istruttore) {
        this.istruttore = istruttore;
    }
    /**
     * Metodo che ritorna il numero massimo di persone frequentanti.
     * @return L'intero che rappresenta il numero di persone frequentanti.
     */
    public int getCapacita() {
        return this.capacita;
    }
    /**
     * Metodo che modifica la capacità del corso.
     * @param capacita Intero che rappresenta la capacità del corso.
     */
    public void setCapacita(int capacita) {
        this.capacita = capacita;
    }
    /**
     * Metodo che ritorna il giorno della settimana quando viene erogato il corso.
     * @return Il giorno della settimana di quando viene erogato il corso.
     */
    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }
    /**
     * Metodo che modifica il giorno della settimana del corso.
     * @param dayOfWeek Giorno della settimana di quando viene erogato il corso.
     */
    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
